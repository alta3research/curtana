FROM python:3.8

# zero interaction while installing or upgrading the system via apt. 
ARG DEBIAN_FRONTEND=noninteractive 

# upgrade pip
RUN pip install -U pip

# Handle dependencies
COPY requirements.txt /tmp
RUN python -m pip install -r /tmp/requirements.txt \
    && pip install -U protobuf==3.20.*

# Create alta3 user and set permissions
RUN useradd alta3 -d /home/alta3 \
    && mkdir /home/alta3 /opt/curtana \
    && chown -R alta3:alta3 /home/alta3 /opt/curtana

WORKDIR /opt/curtana
